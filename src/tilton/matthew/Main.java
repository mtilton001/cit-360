package tilton.matthew;


import tilton.matthew.w08.Week8;

/**
 *
 * Java Main
 *
 * Calls weekly assignments as needed
 *
 * @author Matthew Tilton
 *
 */

public class Main {

    public static void main(String[] args) throws InterruptedException {

        //calls Week2 assignment
        //Collections.main(null);

        //calls Week3 assignment
        //Week3_1.main(null);
        //Week3_2.main(null);
        //Week3_3.main(null);

        //calls Week4 assignment
        //Week4_1.main(null);
        //Week4_2.main(null);

        //calls Week5 assignment but not the JUnit test
        //Week5_1.main(null);

        //calls Week 8 assignments W08.week8, W08.week8_1 and W08.week9_2
        Week8.main(null);
    }
}