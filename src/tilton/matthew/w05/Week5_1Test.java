package tilton.matthew.w05;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * JUnit
 *
 * JUnit: Test methods
 *
 * @author Matthew Tilton
 *
 */

class Week5_1Test {

    //Assertion testing True
    @Test
    public void testTrue() {
        int result = Week5_1.addition(2, 2);
        assertTrue(4 == result);
    }

    //Assertion testing True
    @Test
    public void testFalse() {
        double result = Week5_1.subtract(2, 2);
        assertFalse(5 == result);
    }

    //Assertion testing the String for null value
    @Test
    public void testNull() {
        String message = Week5_1.theNULL(null);
        assertNull(message);
    }

    //Assertion testing the String to verify it is not null
    @Test
    public void testNotNull() {
        String message = Week5_1.theQuestion("Why do birds fly?");
        assertNotNull(message);
    }

    //Assertion testing if the object is the Same
    @Test
    public void testSame() {
        String message = Week5_1.theQuestion("Why do birds fly?");
        assertSame("Why do birds fly?", message);
    }

    //Assertion testing if the object is Not the Same
    @Test
    public void testNotSame() {
        String question = Week5_1.theQuestion("Why do birds fly?");
        String answer = Week5_1.theAnswer("They have wings!");
        assertNotSame(question, answer);
    }

    //Not included in Jupiter. Must use Hamcrest matcher.
    //import org.hamcrest.MatcherAssert : necessary for assertThat
    //import org.hamcrest.Matchers : necessary for equalTo
    @Test
    public void testThat() {
        double result = Week5_1.divide(4.0, 2.0);
        double expected = 4.0 / 2.0;
        //assertThat(expected, equalTo(result));
    }

    //Assertion testing if the multiplied float result is as expected
    @Test
    public void testEquals() {
        float result = Week5_1.multiply(2, 2);
        assertEquals(4, result);
    }

    //Assertion tests expected array against result array
    @Test
    public void testArrayEquals() {
        int[] result = Week5_1.myNumbers();
        int[] expected = {1, 2, 3, 4};
        assertArrayEquals(expected, result);
    }
}