package tilton.matthew.w05;

/**
 *
 * JUnit
 *
 * JUnit: Methods to test
 *
 * @author Matthew Tilton
 *
 */

public class Week5_1 {

    public static void main(String[] args) {

        //just used for testing output and exceptions
        multiply(2, 5);
        addition(9, 8);
        subtract(6, 9);
        divide(15, 3);
    }

    //The arithmetic methods are all interchangeable for assertion testing
    public static float multiply(float x, float y) {
        try{
            System.out.println(x * y);
        }
        catch (ArithmeticException e){
            System.out.println("Please verify variable are numbers!");
        }
        return x * y;
    }

    public static int addition (int x, int y) {
        try{
            System.out.println(x + y);
        }
        catch (ArithmeticException e){
            System.out.println("Please verify variable are numbers!");
        }
        return x + y;
    }

    public static double subtract (double x, double y) {
        try{
            System.out.println(x - y);
        }
        catch (ArithmeticException e){
            System.out.println("Please verify variable are numbers!");
        }
        return x - y;
    }

    public static double divide (double x, double y) {
        try{
            double divide = x / y;
            if (Double.isInfinite(divide) || Double.isNaN(divide)) {
                throw new ArithmeticException();
            }
            System.out.println(x / y);
        }
        catch (ArithmeticException e){
            System.out.println("Please verify variable are numbers!");
        }
        return x / y;
    }


    //The String methods are to test assertions when dealing with Strings
    public static String theQuestion(String question) {
        try {
            if (question.isEmpty()) {
                throw new StringIndexOutOfBoundsException();
            }
        }
        catch (StringIndexOutOfBoundsException e) {
            System.out.println("Please verify question is a String!");
        }
        return question;
    }

    public static String theAnswer(String answer) {
        try {
            if (answer.isEmpty()) {
                throw new StringIndexOutOfBoundsException();
            }
        }
        catch (StringIndexOutOfBoundsException e) {
            System.out.println("Please verify answer is a String!");
        }
        return answer;
    }

    //This String method specifically returns null Week5_1Test input
    public static String theNULL(String NULL) {
        return NULL;
    }

    //The array method is used for assertion ArrayEquals
    public static int[] myNumbers() {
        return new int[]{1, 2, 3, 4};
    }
}