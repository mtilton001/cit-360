package tilton.matthew.w03;

import java.util.*;

/**
 *
 * Java Exception Handling & Data Validation
 *
 * Exception Handling: using int
 *  * ArithmeticException - catches division by 0
 *  * InputMismatchException - catches use of non numeric
 *
 * @author Matthew Tilton
 *
 */

public class Week3_1 {

    public static void main(String[] args) {
        System.out.println("*** Division Calculator ***");

        //Variables created that can used throughout the method
        int numerator;
        int denominator;
        int i = 0;

        //While loop for retries until it exits
        while (i < 4) {
            //Try running this code
            try {
                Scanner numbers = new Scanner(System.in);
                System.out.println("Enter Numerator:");
                numerator = numbers.nextInt();
                System.out.println("Enter Denominator:");
                denominator = numbers.nextInt();

                //This outputs the division calculated from the entered valid numbers
                System.out.println("\n--- " + Calculate(numerator, denominator) + " ---\n");
                i = 4;
            }
            //Catch if InputMismatchException occurs - basically if a non numeric character is entered
            catch (InputMismatchException exception) {
                System.out.println("That is not a number! Please enter a valid number. You have " + (3 - i) + " attempts left!");
                i++;
            }
            //Catch if ArithmeticException occurs - An attempt to divide by 0
            catch (ArithmeticException exception) {
                System.out.println("Please do not try to divide by 0. You have " + (3 - i) + " attempts left!");
                i++;
            }
            //Finally if you've tried 4 times and failed to enter valid numbers you'll get the message below
            finally {
                if (i == 4) {
                    System.out.println("Please try running the program again!");
                }
            }
        }
    }

    //Calculate is the method used to divide the valid numbers input by the user
    public static Integer Calculate(Integer numerator, Integer denominator) throws ArithmeticException, InputMismatchException {
        int divide = numerator / denominator;
        //check for Infinity and NaN: if either thrown exception
        if (Float.isInfinite(divide) || Float.isNaN(divide)) {
            throw new ArithmeticException();
        }
        else {
            return divide;
        }
    }
}