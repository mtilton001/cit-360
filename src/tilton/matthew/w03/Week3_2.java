package tilton.matthew.w03;

import java.util.*;

/**
 *
 * Java Exception Handling & Data Validation
 *
 * Using Float: isInfinite & isNaN to cause exception
 *
 * Exception Handling: throws, try, catch and finally
 *  * ArithmeticException - catches division by 0
 *  * InputMismatchException - catches use of non numeric
 *
 * @author Matthew Tilton
 *
 */

public class Week3_2 {

    public static void main(String[] args) {
        System.out.println("*** Division Calculator ***");

        int i = 0;

        //While loop for retries until it exits
        while (i < 4) {
            //Try running this code
            try {
                Scanner quotient = new Scanner(System.in);
                System.out.println("Enter Numerator:");
                float num1 = quotient.nextFloat();
                System.out.println("Enter Denominator:");
                float num2 = quotient.nextFloat();

                //create var divide to be used for exception handling and system output
                float divide = (Calculate(num1, num2));

                //check for Infinity and NaN: if either thrown exception
                if (Float.isInfinite(divide) || Float.isNaN(divide)) {
                    throw new ArithmeticException();
                }
                else {
                    //This outputs the division calculated from the entered valid numbers
                    System.out.println("\n--- " + divide + " ---\n");
                    i = 5;
                }
            }

            //Catch if InputMismatchException occurs - basically if a non numeric character is entered
            catch (InputMismatchException exception) {
                System.out.println("That is not a number! Please enter a valid number. You have " + (3 - i) + " attempts left!");
                i++;
            }
            //Catch if ArithmeticException occurs - An attempt to divide by 0 : This only works with int not float
            catch (ArithmeticException exception) {
                System.out.println("Please do not try to divide by 0. You have " + (3 - i) + " attempts left!");
                i++;
            }
            //Finally if you've tried 4 times and failed to enter valid numbers you'll get the message below
            finally {
                if (i == 4) {
                    System.out.println("Please try running the program again!");
                }
                else if (i == 5) {
                    System.out.println("Thank you!");
                }
            }
        }
    }

    //Calculate is the method used to divide the valid numbers input by the user
    public static float Calculate(float numerator, float denominator) throws ArithmeticException, InputMismatchException {
        float divide = numerator / denominator;
        //check for Infinity and NaN: if either thrown exception
        if (Float.isInfinite(divide) || Float.isNaN(divide)) {
            throw new ArithmeticException();
        }
        else {
            return divide;
        }
    }
}