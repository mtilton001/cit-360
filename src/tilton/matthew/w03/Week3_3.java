package tilton.matthew.w03;

import java.util.*;

/**
 *
 * Java Exception Handling & Data Validation
 *
 * Exception Handling: Removed to show data validation without exceptions
 *
 * Data Validation: validation through do while loops
 *
 * @author Matthew Tilton
 *
 */

public class Week3_3 {
    public static void main(String[] args) {
        System.out.println("*** Division Calculator ***");

        float num1;
        float num2;

        Scanner quotient = new Scanner(System.in);

        //do while loop for validating user input for num1
        do {
            System.out.println("Enter Numerator:");
            //while loop checks for valid float input from user
            while (!quotient.hasNextFloat()) {
                System.out.println("Please enter a valid number.");
                quotient.next();
            }
            num1 = quotient.nextFloat();
        }
        while (num1 != num1);

        //do while loop for validating user input for num2
        do {
            System.out.println("Enter Denominator:");
            //while loop checks for valid float input from user
            while (!quotient.hasNextFloat()) {
                System.out.println("Please enter a valid number.");
                quotient.next();
            }
            num2 = quotient.nextFloat();
        }
        //This will continue until the user enters a non 0 numeric
        while (num2 == 0);

        //create var divide to be used for exception handling and system output
        float divide = (Calculate(num1, num2));
        System.out.println("\n--- " + divide + " ---\n");
    }

    //Calculate is the method used to divide the valid numbers input by the user
    public static float Calculate(float numerator, float denominator) throws ArithmeticException, InputMismatchException {
        float divide = numerator / denominator;
        //check for Infinity and NaN: if either thrown exception
        if (Float.isInfinite(divide) || Float.isNaN(divide)) {
            throw new ArithmeticException();
        }
        else {
            return divide;
        }
    }
}