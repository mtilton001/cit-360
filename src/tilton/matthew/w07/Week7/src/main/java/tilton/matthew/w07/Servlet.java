package tilton.matthew.w07;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * Servlet
 *
 * Servlet: Used to serve response to either html post or get
 *
 * @author Matthew Tilton
 *
 */

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    //post method
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //String variables set with contents entered on html form
        String date = request.getParameter("date");
        String type = request.getParameter("request");
        String description = request.getParameter("description");

        //Setting response content and encoding
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        //PrintWriter setup with html response data
        PrintWriter write = response.getWriter();
        write.append("      <!DOCTYPE html>")
                .append("   <html>")
                .append("       <head>")
                .append("           <title>Servlet</title>")
                .append("       </head>")
                .append("       <body>")
                .append("           <h2>This is my HTTP post within my Java Servlet</h2>")
                .append("           <p>Date: " + date + "</p>")
                .append("           <p>Request Type: " + type + "</p>")
                .append("           <p>Description: " + description + "</p>")
                .append("       </body>")
                .append("   </html>");
    }

    //get method
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Setting response content and encoding
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        //PrintWriter setup with html response data
        PrintWriter write = response.getWriter();
        write.append("      <!DOCTYPE html>")
                .append("   <html>")
                .append("       <head>")
                .append("           <title>Servlet</title>")
                .append("       </head>")
                .append("       <body>")
                .append("           <h2>This is my http get within my java Servlet</h2>")
                .append("       </body>")
                .append("   </html>");;
    }
}