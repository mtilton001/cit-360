package tilton.matthew.w08;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * Week 8: Threads, Runnables, and Executors
 *
 * main class for Threads, Runnables, and Executors
 *
 * @author Matthew Tilton
 *
 */

public class Week8 {
    public static void main(String[] args) throws InterruptedException {
        int i;

        /*
        ***************************************************
        Threads
            calling thread from extends Thread class
        ***************************************************
        */
        try {
            for (i = 0; i < 15; i++) {
                Week8_1 thread = new Week8_1();
                thread.start();
                i++;
            }
        } catch (Exception e) {
            System.out.println("You have encountered an error in your Thread: Contact your programmer immediately!");
        } finally {
            Thread.sleep(2000);
            System.out.println("\n");
        }

        /*
        ***************************************************
        Runnable:
            calling Runnable threads
        ***************************************************
        */
        try {
            for (i = 0; i < 15; i++) {
                Thread runnableThread = new Thread( new Week8_2() );
                runnableThread.start();
                i++;
            }
        } catch (Exception e) {
            System.out.println("You have encountered an error in your Runnable: Contact your programmer immediately!");
        } finally {
            Thread.sleep(2000);
            System.out.println("\n");
        }

        /*
        ***************************************************
        Runnable:
        Executor:
            Using the ExecutorService with a fixed thread
            pool allows only 3 threads.

            This results in the thread name being 1, 2, or
            3 as there are only 3 threads maximum at an
            given time.
        ***************************************************
        */
        try {
            ExecutorService executorService;
            executorService = Executors.newFixedThreadPool(3);

            for (i = 0; i < 15; i++) {
                Thread executeThread = new Thread(new Week8_2());
                executorService.submit(executeThread);
                i++;
            }
            executorService.shutdown();

        } catch (Exception e) {
            System.out.println("You have encountered an error in your Executor: Contact your programmer immediately!");
        } finally {
            Thread.sleep(2000);
            System.out.println("\n");
        }

        /*
        ***************************************************
        Runnable:
        Executor:
        Atomic Integer:
            calling Runnable threads - up to 5 at a time
        ***************************************************
        */
        try {
            ExecutorService executorService;
            executorService = Executors.newFixedThreadPool(5);
            Week8_3 count = new Week8_3();

            for (i = 0; i < 25; i++) {
                Thread executeThread = new Thread( new Week8_3() );
                executorService.submit(executeThread);
                count.incrementAtomic();
            }

            executorService.shutdown();
            Thread.sleep(1000);

        } catch (Exception e) {
            System.out.println("You have encountered an error in your Atomic Integer: Contact your programmer immediately!");
        }

        System.out.println("\nTh-th-th-th-That's all Folks!");
    }
}