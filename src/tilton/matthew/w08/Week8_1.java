package tilton.matthew.w08;

/**
 *
 * Week 8: Threads, Runnables, Executors
 *
 * Thread subclass: week8_1 extends Thread
 *
 * @author Matthew Tilton
 *
 */

public class Week8_1 extends Thread {
    public void run() {
        String getThreadName = Thread.currentThread().getName();

        System.out.println(getThreadName + " is running");
        System.out.println(getThreadName + " has finished");
    }
}