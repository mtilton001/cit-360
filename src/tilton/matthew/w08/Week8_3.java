package tilton.matthew.w08;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * Week 8: Threads, Runnables, Executors
 *
 * implements Runnable: week8_3 with AtomicInteger
 *
 * @author Matthew Tilton
 *
 */

public class Week8_3 implements Runnable {
    public AtomicInteger myAtomic = new AtomicInteger(0);

    public int incrementAtomic() {
        return myAtomic.incrementAndGet();
    }

    public int getAtomic() {
        return myAtomic.get();
    }

    @Override
    public void run() {
        String getThreadName = Thread.currentThread().getName();

        System.out.println("Atomic " + getThreadName + " is running ");
        System.out.println("Atomic " + getThreadName + " has finished ");
    }
}