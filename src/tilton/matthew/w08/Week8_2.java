package tilton.matthew.w08;

/**
 *
 * Week 8: Threads, Runnables, Executors
 *
 * implements Runnable: week8_2
 *
 * @author Matthew Tilton
 *
 */

public class Week8_2 implements Runnable {
    @Override
    public void run() {
        String getThreadName = Thread.currentThread().getName();

        System.out.println("Runnable " + getThreadName + " is running ");
        System.out.println("Runnable " + getThreadName + " has finished");
    }
}