package tilton.matthew.w04;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;

import java.net.InetSocketAddress;

/**
 *
 * Java JSON and HTTP or URL
 *
 * HTTP Server for Object for JSON
 *
 * @author Matthew Tilton
 *
 */

public class Week4_1 {

    public static void main(String[] args) {
        try {
            //creates HTTP server instance and binds HTTP port 80 with a backlog of 0 or backlog of system default
            //The following link has good information about backlog but I couldn't find what the system default is
            //https://docs.oracle.com/javase/8/docs/jre/api/net/httpserver/spec/com/sun/net/httpserver/HttpServer.html
            HttpServer myWebServer = HttpServer.create(new InetSocketAddress(80), 0);
            //The context maps the URI (Universal Resource Identifier) path to a HTTP handler. In this case the URI or path is http://localhost/
            HttpContext myURI = myWebServer.createContext("/");
            //Using the :: the handler set to this class (Week4_1) and the handlerExchange method
            myURI.setHandler(Week4_1::handlerExchange);
            //Starts web server
            myWebServer.start();

            System.out.println("Server starting -->");
            Thread.sleep(5000);
            System.out.println("\nServer started Successfully!\n" +
                    "\nPlease click http://localhost to open serer webpage.");
        }
        catch(IOException | InterruptedException e) {
            System.out.println("The web server has not successfully started:");
        }
    }

    //handlerExchange method creates new MartialArtRanks object then enters into the OutputStream
    private static void handlerExchange(HttpExchange exchange) {

        try {
            //Create JSON from object
            MartialArtRanks newRank = new MartialArtRanks();
            newRank.setPid(1001);
            newRank.setGroup("Basic");
            newRank.setRank("Green");

            //String for output to web page
            String myWebOutput = ObjectToJSON(newRank);
            //Console output: to show what the web server displays (when a client connects)
            System.out.println("\nSERVER: From Week4_1 class handlerExchange method\n" + myWebOutput);

            //Response is using 0 for the rCode which allows arbitrary data length
            exchange.sendResponseHeaders(0, myWebOutput.getBytes().length);
            //Makes use of the OutputStream class
            OutputStream myStream = exchange.getResponseBody();
            //Writes bytes (data) to stream
            myStream.write(myWebOutput.getBytes());
            //Closes stream
            myStream.close();
        }
        catch (IOException e) {
            System.out.println("Your JSON from Object MartialArtRanks is not available!");
        }
    }

    //ObjectToJSON method uses data processed from handlerExchange method through MartialArtRanks class
    public static String ObjectToJSON(MartialArtRanks martialArtRanks) {

        //initializes objectToJSON
        String objectToJSON = null;

        //Creates new objectMapper then uses mapper to create JSON from object
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectToJSON = objectMapper.writeValueAsString(martialArtRanks);
        }
        catch (JsonProcessingException e) {
            System.out.println("JSON from Java object failed!");
        }

        return objectToJSON;
    }

    //Public class to define object
    public static class MartialArtRanks {

        private int pid;
        private String group;
        private String rank;

        //Getters and Setters
        public int getPid() {
            return pid;
        }

        public void setPid(int pid) {
            this.pid = pid;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        //toString for pid, group & rank (properties of martialArtRanks class object)
        public String toString() {
            return "PID: " + pid + "\nGroup: " + group + "\nRank: " + rank;
        }
    }
}