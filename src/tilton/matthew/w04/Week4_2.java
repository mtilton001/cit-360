package tilton.matthew.w04;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import java.net.URL;

/**
 *
 * Java JSON and HTTP or URL
 *
 * Client uses localhost URL for JSON to Object
 *
 * @author Matthew Tilton
 *
 */

public class Week4_2 {

    public static void main(String[] args) {

        System.out.println(JSONtoObject("http://localhost/"));
    }

    public static Week4_1.MartialArtRanks JSONtoObject (String string) {


        Week4_1.MartialArtRanks content = null;

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            URL url = new URL(string);

            content = objectMapper.readValue(url, Week4_1.MartialArtRanks.class);
        }
        catch(IOException e) {
            System.out.println(e.toString());
        }
        System.out.println("\nCLIENT: From Week4_2 class main method");
        return content;
    }
}