package tilton.matthew.w02;

import java.util.*;

/**
 *
 * Java Collections
 *
 * --------Comparator
 *
 * @author Matthew Tilton
 *
 */

public class CompTool implements Comparator<Double> {

    @Override
    public int compare(Double o1, Double o2) {

        if (o1%1 > o2%1)
            return 0;
        return -1;
    }
}
