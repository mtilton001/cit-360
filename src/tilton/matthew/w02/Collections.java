package tilton.matthew.w02;

import java.util.*;

/**
 *
 * Java Collections
 *
 * 5 basic types of collections: List, Set, Map, Queue, Tree
 *
 * @author Matthew Tilton
 *
 */

public class Collections {

    public static void main(String[] args) {

        try {

            //A list allows modification based on indices (integer index).
            //Names of my children
            System.out.println("List Collection (String)");
            List<String> myChildren = new ArrayList<>();
            myChildren.add("Kailey");
            myChildren.add("Jenika");
            myChildren.add("Madison");
            myChildren.add("Jace");
            myChildren.add("Kinzey");

            for (String myChild : myChildren) {
                System.out.println(myChild);
            }

            //Numbers List
            //Numbers List using Comparator
            System.out.println("\nList Collection (Double)");
            List<Double> myNumbers = new ArrayList<>();
            myNumbers.add(10.34);
            myNumbers.add(25.87);
            myNumbers.add(17.96);
            myNumbers.add(33.26);
            myNumbers.add(99.12);
            System.out.println(myNumbers);

            System.out.println("\nComparator: Ordered by tenths decimal");
            Comparator<Double> comparing = new CompTool();
            //Collections.sort(myNumbers,comparing);
            java.util.Collections.sort(myNumbers, comparing);
            for (Double i : myNumbers) {
                System.out.println(i);
            }

            //Set cannot contain duplicates.
            //Members of my group (Group 7).
            System.out.println("\nSet Collection (String)");
            Set<String> myGroup = new HashSet<>();
            myGroup.add("Brycen");
            myGroup.add("David");
            myGroup.add("Eduardo");
            myGroup.add("Marcus");
            myGroup.add("Matias");
            myGroup.add("Matthew");
            myGroup.add("David");

            for (String getGroup : myGroup) {
                System.out.println(getGroup);
            }

            //A map cannot contain duplicates and each key maps only one value.
            //Skittles per bag (about 25)
            System.out.println("\nMap Collection (String, Integer)");
            Map<String, Integer> countByColor = new TreeMap<>();
            countByColor.put("Green", 4);
            countByColor.put("Yellow", 5);
            countByColor.put("Orange", 3);
            countByColor.put("Red", 6);
            countByColor.put("Purple", 8);

            for (Map.Entry<String, Integer> skittleCount : countByColor.entrySet())
                System.out.println(skittleCount);

            //A queue allows modifications and hold elements before processing.
            //# of shoppers ready for checkout.
            System.out.println("\nQueue Collection (String)");
            Queue<String> shoppers = new PriorityQueue<>();
            shoppers.add("Iron Man");
            shoppers.add("Thor");
            shoppers.add("Black Widow");
            shoppers.add("Hulk");
            shoppers.add("Captain America");
            shoppers.add("Star Lord");

            System.out.println(shoppers.toString() + "\n");

            Iterator checkoutLine = shoppers.iterator();
            while (checkoutLine.hasNext()) {
                System.out.println(shoppers.poll());
            }

            //TreeSet is very much like set but automatically orders them.
            //Components of a tree
            System.out.println("\nTree Collection (String)");
            TreeSet<String> leafMeAlone = new TreeSet<>();
            leafMeAlone.add("Trunk");
            leafMeAlone.add("Bark");
            leafMeAlone.add("Leaves");
            leafMeAlone.add("Branch");
            leafMeAlone.add("Roots");

            for (String treeRing : leafMeAlone) {
                System.out.println(treeRing);
            }

            System.out.println("\nMyCollection list with Generics");
            List<MyCollections> inventory = new ArrayList<>();

            inventory.add(new MyCollections(1001, " Book   ", "Adventurers Wanted: Slathbogs Gold        ", "Bookcase 1 Self 3"));
            inventory.add(new MyCollections(1002, " Movie  ", "Golden Eye                                ", "Netflix"));
            inventory.add(new MyCollections(1003, " Book   ", "Adventurers Wanted: The Horn of Moran     ", "Bookcase 2 Self 4"));
            inventory.add(new MyCollections(1004, " Car    ", "Chevrolet Tahoe                           ", "Driveway Lane 1 Position 1"));
            inventory.add(new MyCollections(1005, " Book   ", "Adventurers Wanted: Albreks Tomb          ", "Bookcase 1 Self 2"));
            inventory.add(new MyCollections(1006, " Book   ", "Adventurers Wanted: Sands of Nezza        ", "Bookcase 1 Self 1"));
            inventory.add(new MyCollections(1007, " Book   ", "Adventurers Wanted: The Axe of Sundering  ", "Bookcase 3 Self 5"));
            inventory.add(new MyCollections(1008, " Movie  ", "No Time to Die                            ", "Movies Anywhere"));
            inventory.add(new MyCollections(1009, " Movie  ", "Quantum of Solace                         ", "Amazon Prime"));
            inventory.add(new MyCollections(1010, " Car    ", "Volkswagen Jetta                          ", "Driveway Lane 1 Position 2"));

            for (MyCollections items : inventory) {
                System.out.println(items);
            }

        } catch (Exception e) {
            System.out.println("Everything is hardcoded! What could really be the problem? :)");
        } finally {
            System.out.println("\nYeah! No Exceptions! try catch, out!");
        }
    }
}
