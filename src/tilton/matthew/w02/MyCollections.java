package tilton.matthew.w02;

/**
 *
 * Java Collections
 *
 * --------MyCollections
 *
 * @author Matthew Tilton
 *
 */

public class MyCollections {

    //Item#: PID or Inventory ID
    private Integer item;
    //Category: Category Type - Book, Movie, Cars
    private String category;
    //Name: Title or name of item
    private String name;
    //Location: Physical location
    private String location;

    public MyCollections(Integer item, String category, String name, String location) {
        this.item = item;
        this.category = category;
        this.name = name;
        this.location = location;
    }

    public String toString(){
        return "Item - " + item + " | Category - " + category + " | Name - " + name + " | Location - " + location;
    }
}
